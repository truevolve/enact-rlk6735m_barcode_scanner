//package com.truevolve.android.enact.ax6737_barcode_scanner
//
//import android.content.Context
//import android.content.Intent
//
//const val SCAN_QR = "scan_qr"
//const val SCAN_PDF417 = "scan_pdf417"
//const val SCAN_DATAMATRIX = "scan_datamatrix"
//const val SCAN_AZTEC = "scan_aztec"
//const val SCAN_CODE_128 = "SCAN_CODE_128"
//const val SCAN_GS1_128 = "SCAN_GS1_128"
//const val SCAN_INTERLEAVED_25 = "SCAN_INTERLEAVED_25"
//const val SCAN_CODABAR = "SCAN_CODABAR"
//const val SCAN_EAN_13 = "SCAN_EAN_13"
//const val SCAN_UPC_A_ = "SCAN_UPC_A_"
//const val SCAN_CODE_39 = "SCAN_CODE_39"
//
//val PROPERTY_CODE_128_ENABLED = "DEC_CODE128_ENABLED"
//val PROPERTY_GS1_128_ENABLED = "DEC_GS1_128_ENABLED"
//val PROPERTY_CODE_39_ENABLED = "DEC_CODE39_ENABLED"
//val PROPERTY_CODE_39_MAXIMUM_LENGTH = "DEC_CODE39_MAX_LENGTH"
//val PROPERTY_DATAMATRIX_ENABLED = "DEC_DATAMATRIX_ENABLED"
//val PROPERTY_UPC_A_ENABLE = "DEC_UPCA_ENABLE"
//val PROPERTY_EAN_13_ENABLED = "DEC_EAN13_ENABLED"
//val PROPERTY_AZTEC_ENABLED = "DEC_AZTEC_ENABLED"
//val PROPERTY_CODABAR_ENABLED = "DEC_CODABAR_ENABLED"
//val PROPERTY_INTERLEAVED_25_ENABLED = "DEC_I25_ENABLED"
//val PROPERTY_PDF_417_ENABLED = "DEC_PDF417_ENABLED"
//val PROPERTY_QR_CODE_ENABLED = "DEC_QR_ENABLED"
//
//class ScanUtil(private val context: Context) {
//
//    private val ACTION_ENABLE_SCAN_SERVER = "com.rfid.ENABLE_SCAN_SERVER"
//    private val ACTION_SCAN_CMD = "com.rfid.SCAN_CMD"
//    private val ACTION_DISABLE_SCAN_SERVER = "com.rfid.DISABLE_SCAN_SERVER"
//
//    init {
//
//        val intent = Intent()
//
//        intent.action = ACTION_ENABLE_SCAN_SERVER
//        intent.putExtra(PROPERTY_CODE_128_ENABLED, SCAN_CODE_128)
//        intent.putExtra(PROPERTY_GS1_128_ENABLED, SCAN_GS1_128)
//        intent.putExtra(PROPERTY_QR_CODE_ENABLED, SCAN_QR)
//        intent.putExtra(PROPERTY_CODE_39_ENABLED, SCAN_CODE_39)
//        intent.putExtra(PROPERTY_DATAMATRIX_ENABLED, SCAN_DATAMATRIX)
//        intent.putExtra(PROPERTY_UPC_A_ENABLE, SCAN_UPC_A_)
//        intent.putExtra(PROPERTY_EAN_13_ENABLED, SCAN_EAN_13)
//        intent.putExtra(PROPERTY_AZTEC_ENABLED, SCAN_AZTEC)
//        intent.putExtra(PROPERTY_CODABAR_ENABLED, SCAN_CODABAR)
//        intent.putExtra(PROPERTY_INTERLEAVED_25_ENABLED, SCAN_INTERLEAVED_25)
//        intent.putExtra(PROPERTY_PDF_417_ENABLED, SCAN_PDF417)
//        intent.putExtra(PROPERTY_CODE_39_MAXIMUM_LENGTH, 10)
//        context.sendBroadcast(intent)
//    }
//
//    fun scan() {
//        val intent = Intent()
//        intent.action = ACTION_SCAN_CMD
//        context.sendBroadcast(intent)
//    }
//
//    fun close() {
//        val toKillService = Intent()
//        toKillService.action = ACTION_DISABLE_SCAN_SERVER
//        context.sendBroadcast(toKillService)
//    }
//}
