package com.truevolve.android.enact.ax6737_barcode_scanner

import android.content.Context
import android.content.Intent

/**
 * Created by Administrator on 2018/4/19.
 */

class ScanUtil(private val context: Context) {

    private val ACTION_SCAN_INIT = "com.rfid.SCAN_INIT"
    private val ACTION_SCAN_CMD = "com.rfid.SCAN_CMD"
    private val ACTION_KILL_SCAN= "com.rfid.KILL_SCAN"
    val ACTION_CLOSE_SCAN = "com.rfid.CLOSE_SCAN" //ACTION_SCAN_CMD

    val ACTION_SET_SCAN_MODE = "com.rfid.SET_SCAN_MODE" //ACTION_SCAN_CMD

    val ACTION_ENABLE_SYM = "com.rfid.ENABLE_SYM" //ACTION_SCAN_CMD

    init {
        val intent = Intent()
        intent.action = ACTION_SCAN_INIT
        context.sendBroadcast(intent)
    }

    /**
     * start scan
     */
    fun scan() {
        val intent = Intent()
        intent.action = ACTION_SCAN_CMD
        context.sendBroadcast(intent)
    }

    fun setScanMode(mode: Int) {
        val intent = Intent()
        intent.action = ACTION_SET_SCAN_MODE
        intent.putExtra("mode", mode)
        context.sendBroadcast(intent)
    }

    fun setScanSym(symValue: String, isEnable: Boolean) {
        val setScanSym = Intent()
        setScanSym.action = ACTION_ENABLE_SYM
        setScanSym.putExtra("symbology", symValue)
        setScanSym.putExtra("enable", isEnable)
        context.sendBroadcast(setScanSym)
    }


    fun close() {
        val intent = Intent()
        intent.action = ACTION_CLOSE_SCAN
        intent.putExtra("iscamera", true)
        context.sendBroadcast(intent)
    }

    object SymbologyValues {
        val SYM_AZTEC = "sym_aztec_enable"
        val SYM_CODABAR = "sym_codabar_enable"
        val SYM_CODABLOCK = "sym_codablock_enable"
        val SYM_CODE11 = "sym_code11_enable"
        val SYM_CODE128 = "sym_code128_enable"
        val SYM_CODE32 = "sym_code32_enable"
        val SYM_CODE39 = "sym_code39_enable"
        val SYM_CODE49 = "sym_code49_enable"
        val SYM_CODE93 = "sym_code93_enable"
        val SYM_COMPOSITE = "sym_composite_enable"
        val SYM_COUPONCODE = "sym_couponcode_enable"
        val SYM_DATAMATRIX = "sym_datamatrix_enable"
        val SYM_EAN8 = "sym_ean8_enable"
        val SYM_EAN13 = "sym_ean13_enable"
        val SYM_GS1_128 = "sym_gs1_128_enable"
        val SYM_HANXIN = "sym_hanxin_enable"
        val SYM_IATA25 = "sym_iata25_enable"
        val SYM_INT25 = "sym_int25_enable"
        val SYM_ISBT = "sym_isbt_enable"
        val SYM_MATRIX25 = "sym_matrix25_enable"
        val SYM_MAXICODE = "sym_maxicode_enable"
        val SYM_MICROPDF = "sym_micropdf_enable"
        val SYM_MSI = "sym_msi_enable"
        val SYM_PDF417 = "sym_pdf417_enable"
        val SYM_QR = "sym_qr_enable"
        val SYM_RSS = "sym_rss_rss_enable"
        val SYM_STRT25 = "sym_strt25_enable"
        val SYM_TELEPEN = "sym_telepen_enable"
        val SYM_TLCODE39 = "sym_tlcode39_enable"
        val SYM_TRIOPTIC = "sym_trioptic_enable"
        val SYM_UPCA = "sym_upca_enable"
        val SYM_UPCE0 = "sym_upce0_enable"
        val SYM_UPCE1 = "sym_upce1_upce1_enable"

    }
}
