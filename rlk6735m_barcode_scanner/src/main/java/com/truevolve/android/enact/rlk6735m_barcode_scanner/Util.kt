package com.truevolve.android.enact.rlk6735m_barcode_scanner

import android.content.Context
import android.media.AudioManager
import android.media.SoundPool
import android.util.SparseIntArray


object Util {


    var sp: SoundPool? = null
    private var soundMap: SparseIntArray? = null
    var context: Context? = null

    //
    fun initSoundPool(context: Context) {
        sp = SoundPool(1, AudioManager.STREAM_MUSIC, 1)
        sp?.let {
            Util.context = context
            soundMap = SparseIntArray()
            soundMap?.put(1, it.load(context, R.raw.msg, 1))
        }
    }

    //
    fun play(sound: Int, number: Int) {
        soundMap?.let {
            val am = Util.context?.getSystemService(Context.AUDIO_SERVICE) as AudioManager
            //
            val audioMaxVolume = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC).toFloat()

            //
            val audioCurrentVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC).toFloat()
            sp?.play(
                    it.get(sound), //
                    audioCurrentVolume, //
                    audioCurrentVolume, //
                    1,
                    number,
                    1f)//
        }
    }

}
