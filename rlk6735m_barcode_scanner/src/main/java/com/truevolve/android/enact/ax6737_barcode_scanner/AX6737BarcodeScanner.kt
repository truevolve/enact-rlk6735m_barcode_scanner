package com.truevolve.android.enact.ax6737_barcode_scanner

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v7.widget.AppCompatImageView
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import com.truevolve.android.enact.rlk6735m_barcode_scanner.R
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.PolicyException
import org.json.JSONException
import org.json.JSONObject

class AX6737BarcodeScanner : ActivityBaseController() {
    override val type: String
        get() = TYPE

    override fun validate(stateObj: JSONObject) {
    }

    private val DISPLAY_MESSAGE = "display_message"
    private val STORE_AS = "store_as"
    private val ON_SCANNED = "on_scanned"
    private val ON_CANCEL = "on_cancel"
    private val CANCEL_TEXT = "cancel_text"
    // cancel text is optional
    private val TAG = "BarcodeScanner"
    private val BUTTON_IMAGE = "button_image"

    private val TYPE = "ax6737_barcode_scanner"

    private val RESOURCE_TO_LOAD = "resource_to_load" //optional to specify which layout file should be inflated.
    private var onScannedState: String? = null
    private var storeAsKey: String? = null
    private var onCancelState: String? = null


    // AX6737 variables

    private var scanButton: Button? = null
    private var progressSpinner: ProgressBar? = null

    private var scanUtil: ScanUtil? = null


    // Listener for event broadcast from scan service
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            val data = intent.getByteArrayExtra("data")

            Log.d(TAG, "BroadcastReceiver -> " + String(data))

            if (data != null) {
                scanUtil?.close()

                stateObj?.let {
                    Interpreter.dataStore.put(it.getString(STORE_AS), data);
                    goToState(it.getString(ON_SCANNED));
                }

            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {

            // Register listener for scan
            val filter = IntentFilter()
            filter.addAction("com.rfid.SCAN")
            registerReceiver(receiver, filter)

            if (stateObj?.has(RESOURCE_TO_LOAD) == true && stateObj?.getString(RESOURCE_TO_LOAD)?.contains("BACKGROUND_AND_BASICS") == true) {
                setContentView(R.layout.activity_background_and_basics)

                //Now we check if the policy specifies what object (driver card/vehicle disk) we have to scan.
                try {
                    if (stateObj?.has(BUTTON_IMAGE) == true && stateObj?.getString(BUTTON_IMAGE).equals("DRIVER_CARD", ignoreCase = true)) {
                        val imageView = findViewById<AppCompatImageView>(R.id.imageView2)
                        imageView.setImageResource(R.drawable.ic_scan_driver_card_white)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            } else {
                setContentView(R.layout.activity_for_justin)

                //Now we check if the policy specifies what object (driver card/vehicle disk) we have to scan.
                try {
                    if (stateObj?.has(BUTTON_IMAGE) == true && stateObj?.getString(BUTTON_IMAGE).equals("DRIVER_CARD", ignoreCase = true)) {
                        findViewById<View>(R.id.scanBarcode).setBackgroundResource(R.drawable.scan_driver_card_button)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }


        val displayMessage = findViewById<TextView>(R.id.displayMessageText)
        try {
            displayMessage.text = stateObj?.getString(DISPLAY_MESSAGE)
            onScannedState = stateObj?.getString(ON_SCANNED)
            onCancelState = stateObj?.getString(ON_CANCEL)
            storeAsKey = stateObj?.getString(STORE_AS)
        } catch (e: JSONException) {
            e.printStackTrace()
            Interpreter.error(this, e)
        }

        scanButton = findViewById(R.id.scanBarcode)
        scanButton?.setOnClickListener {
            scanUtil?.let {
                setupScanUtil()
                it.scan()
            }
        }

        progressSpinner = findViewById(R.id.progressSpinner)

        val cancelButton = findViewById<Button>(R.id.cancelButton)

        if (stateObj?.has(CANCEL_TEXT) == true) {
            try {
                cancelButton.text = stateObj?.getString(CANCEL_TEXT)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }

        cancelButton.setOnClickListener {
            try {
                onCancelState?.let {
                    goToState(it)
                }
            } catch (e: InterpreterException) {
                e.printStackTrace()
                Interpreter.error(this@AX6737BarcodeScanner, e)
            } catch (e: JSONException) {
                e.printStackTrace()
                Interpreter.error(this@AX6737BarcodeScanner, e)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        val filter = IntentFilter()
        filter.addAction("android.rfid.FUN_KEY")


        if (scanUtil == null) {
            setupScanUtil()

            val filter = IntentFilter()
            filter.addAction("com.rfid.SCAN")
            registerReceiver(receiver, filter)
        }

    }

    private fun setupScanUtil(){
        scanUtil = ScanUtil(this)
        scanUtil?.setScanMode(0)
        scanUtil?.setScanSym(ScanUtil.SymbologyValues.SYM_PDF417, true) //open pdf417 code
        scanUtil?.setScanSym(ScanUtil.SymbologyValues.SYM_CODE39, true) //open code39 code
    }

    override fun onPause() {
        Log.d(TAG, "onPause: starting")
        super.onPause()

        if (scanUtil != null) {
            scanUtil?.close()
            scanUtil = null
            unregisterReceiver(receiver)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        Log.d(TAG, "onKeyDown: starting")

        //start scan while press down funkey,the value range: 131~135
        if (keyCode in 131..135 || keyCode == 19) {
            scanUtil?.let {
                it.setScanMode(0)//mode :0 , broadcast mode， 1， text edit input
                it.scan()
            }
        }
        return super.onKeyDown(keyCode, event)
    }

}
