package com.truevolve.android.enact.rlk6735m_barcode_scanner

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import com.truevolve.android.enact.ax6737_barcode_scanner.AX6737BarcodeScanner
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.PolicyException
import org.json.JSONException
import org.json.JSONObject

class BarcodeScannerEntryPoint : ActivityBaseController() {
    override val type: String
        get() = TYPE

    private val TYPE = "rlk6735m_barcode_scanner"

    private val DISPLAY_MESSAGE = "display_message"
    private val STORE_AS = "store_as"
    private val ON_SCANNED = "on_scanned"
    private val ON_CANCEL = "on_cancel"
    // cancel text is optional
    private val TAG = "BarcodeScanner"
    private val SCAN_QR = "scan_qr"
    private val SCAN_DATAMATRIX = "scan_datamatrix"
    private val SCAN_PDF417 = "scan_pdf417"
    private val SCAN_CODE39 = "scan_code39"

    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    protected override fun onResume() {
        super.onResume()

        Log.d("LOG", "Model " + Build.MODEL)

        val intent = when {
            Build.MODEL.contains("ax6737", true) -> {
                Intent(this, AX6737BarcodeScanner::class.java)
            }
            else -> {
                Intent(this, RLK6735BarcodeScanner::class.java)
            }
        }
        intent.putExtras(getIntent())
        startActivity(intent)
    }

    protected override fun onPause() {

        super.onPause()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return true
    }


    private fun scan(timeout: Int) {

    }

    @Throws(JSONException::class)
    internal fun settingPara() {

    }

    @Throws(PolicyException::class)
    override fun validate(stateObj: JSONObject) {
        if (!stateObj.has(DISPLAY_MESSAGE)) {
            Log.e(TAG, "validate: $TYPE's $DISPLAY_MESSAGE was not specified.")
            throw PolicyException("$TYPE's $DISPLAY_MESSAGE was not specified.")

        } else if (!stateObj.has(STORE_AS)) {
            Log.e(TAG, "validate: $TYPE's $STORE_AS was not specified.")
            throw PolicyException("$TYPE's $STORE_AS was not specified.")

        } else if (!stateObj.has(ON_SCANNED)) {
            Log.e(TAG, "validate: $TYPE's $ON_SCANNED was not specified.")
            throw PolicyException("$TYPE's $ON_SCANNED was not specified.")


        } else if (!stateObj.has(ON_CANCEL)) {
            Log.e(TAG, "validate: $TYPE's $ON_CANCEL was not specified.")
            throw PolicyException("$TYPE's $ON_CANCEL was not specified.")

        } else
            try {
                if (!(stateObj.has(SCAN_DATAMATRIX) && stateObj.getBoolean(SCAN_DATAMATRIX)
                                || stateObj.has(SCAN_QR) && stateObj.getBoolean(SCAN_QR)
                                || stateObj.has(SCAN_CODE39) && stateObj.getBoolean(SCAN_CODE39)
                                || stateObj.has(SCAN_PDF417) && stateObj.getBoolean(SCAN_PDF417))) {

                    Log.e(TAG, "validate: $TYPE must have at least one of $SCAN_DATAMATRIX, $SCAN_QR, $SCAN_CODE39 or $SCAN_PDF417  specified.")
                    throw PolicyException("$TYPE must have at least one of $SCAN_DATAMATRIX, $SCAN_QR, $SCAN_CODE39 or $SCAN_PDF417  specified.")
                }
            } catch (e: JSONException) {
                e.printStackTrace()
                throw PolicyException(TYPE + " must have at least one of " + SCAN_DATAMATRIX +
                        ", " + SCAN_QR + ", " + SCAN_CODE39 + " or " + SCAN_PDF417 + "  specified." + e.message)

            }

    }
}