package com.truevolve.android.enact.rlk6735m_barcode_scanner

import android.content.IntentFilter
import android.os.Bundle
import android.support.v7.widget.AppCompatImageView
import android.util.Base64
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import com.hsm.barcode.*
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.PolicyException
import org.json.JSONException
import org.json.JSONObject

class RLK6735BarcodeScanner : ActivityBaseController() {

    private val RESOURCE_TO_LOAD = "resource_to_load" //optional to specify which layout file should be inflated.
    private var onScannedState: String? = null
    private var storeAsKey: String? = null
    private var onCancelState: String? = null


    /******
     * BH84 variables
     */

    internal var scanning = false
    internal var running = true
    private var mDecoder: Decoder? = null
    private var mDecodeResult: DecodeResult? = null
    private val threadRunning = false
    private val timeOut = 5000
    private var scanButton: Button? = null
    private var progressSpinner: ProgressBar? = null

    private var stopping = false

    override val type: String
        get() = TYPE


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {

            if (stateObj?.has(RESOURCE_TO_LOAD) == true && stateObj?.getString(RESOURCE_TO_LOAD)?.contains("BACKGROUND_AND_BASICS") == true) {
                setContentView(R.layout.activity_background_and_basics)

                //Now we check if the policy specifies what object (driver card/vehicle disk) we have to scan.
                try {
                    if (stateObj?.has(BUTTON_IMAGE) == true && stateObj?.getString(BUTTON_IMAGE).equals("DRIVER_CARD", ignoreCase = true)) {
                        findViewById<AppCompatImageView>(R.id.imageView2).setImageResource(R.drawable.ic_scan_driver_card_white)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            } else {
                setContentView(R.layout.activity_for_justin)

                //Now we check if the policy specifies what object (driver card/vehicle disk) we have to scan.
                try {
                    if (stateObj?.has(BUTTON_IMAGE) == true && stateObj?.getString(BUTTON_IMAGE).equals("DRIVER_CARD", ignoreCase = true)) {
                        findViewById<View>(R.id.scanBarcode).setBackgroundResource(R.drawable.scan_driver_card_button)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }


        try {
            findViewById<TextView>(R.id.displayMessageText).text = stateObj?.getString(DISPLAY_MESSAGE)
            onScannedState = stateObj?.getString(ON_SCANNED)
            onCancelState = stateObj?.getString(ON_CANCEL)
            storeAsKey = stateObj?.getString(STORE_AS)
        } catch (e: JSONException) {
            e.printStackTrace()
            Interpreter.error(this, e)
        }

        scanButton = findViewById(R.id.scanBarcode)
        scanButton?.setOnClickListener { scan(timeOut) }

        progressSpinner = findViewById(R.id.progressSpinner)

        val cancelButton = findViewById<Button>(R.id.cancelButton)

        if (stateObj?.has(CANCEL_TEXT) == true) {
            try {
                cancelButton.text = stateObj?.getString(CANCEL_TEXT)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }

        cancelButton.setOnClickListener {
            try {
                onCancelState?.let {
                    goToState(it)
                }
            } catch (e: InterpreterException) {
                e.printStackTrace()
                Interpreter.error(this@RLK6735BarcodeScanner, e)
            } catch (e: JSONException) {
                e.printStackTrace()
                Interpreter.error(this@RLK6735BarcodeScanner, e)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        val filter = IntentFilter()
        filter.addAction("android.rfid.FUN_KEY")

    }

    override fun onPause() {
        Log.d(TAG, "onPause: starting")

        if (!stopping) {
            Thread(Runnable {
                try {
                    stopping = true

                    while (scanning) {
                        Thread.sleep(100)
                    }

                    try {
                        mDecoder?.disconnectDecoderLibrary()
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Log.e(TAG, "onPause: Failed to deregister receiver", e)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }).start()
        }

        super.onPause()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        Log.d(TAG, "onKeyDown: starting")

        //start scan while press down funkey,the value range: 131~135
        if (keyCode in 131..135 || keyCode == 19) {
            if (!threadRunning) {
                scan(timeOut)
            }
        }
        return super.onKeyDown(keyCode, event)
    }


    private fun scan(timeout: Int) {

        // RLK6735M stuff

        mDecodeResult = DecodeResult()
        Util.initSoundPool(this)
        running = true
        mDecoder = Decoder()

        //End of RLK6735M stuff

        scanButton?.visibility = View.INVISIBLE
        progressSpinner?.visibility = View.VISIBLE

        if (!scanning) {
            Thread(Runnable {
                scanning = true
                try {
                    mDecoder?.connectDecoderLibrary()
                    settingPara()

                    mDecoder?.waitForDecodeTwo(timeout, mDecodeResult)

                    mDecodeResult?.length?.let {
                        if (it > 0) {
                            scanning = false

                            Util.play(1, 0)
                            val tt = mDecoder?.barcodeByteData

                            Log.d(TAG, "run: barcode raw size is: " + Base64.encodeToString(tt, Base64.DEFAULT))

                            scanning = false
                            mDecoder?.disconnectDecoderLibrary()

                            stateObj?.let {
                                tt?.let { barcode ->
                                    Interpreter.dataStore.put(it.getString(STORE_AS), barcode)
                                    goToState(it.getString(ON_SCANNED))
                                }
                            }
                        }
                    }
                    scanning = false

                    mDecoder?.disconnectDecoderLibrary()

                    runOnUiThread {
                        scanButton?.visibility = View.VISIBLE
                        progressSpinner?.visibility = View.INVISIBLE
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    Log.e(TAG, "onPause: Failed to deregister receiver", e)
                    scanning = false

                    try {
                        mDecoder?.disconnectDecoderLibrary()
                    } catch (e1: DecoderException) {
                        e1.printStackTrace()
                    }

                    runOnUiThread {
                        scanButton?.visibility = View.VISIBLE
                        progressSpinner?.visibility = View.INVISIBLE
                    }
                }
            }).start()
        }
    }

    @Throws(JSONException::class)
    internal fun settingPara() {
        try {
            val config = SymbologyConfig(DecoderConfigValues.SymbologyID.SYM_EAN13)
            config.Flags = 5
            config.Mask = 1
            mDecoder?.setSymbologyConfig(config)
            mDecoder?.disableSymbology(DecoderConfigValues.SymbologyID.SYM_ALL)
            mDecoder?.enableSymbology(DecoderConfigValues.SymbologyID.SYM_ALL)

            if (stateObj?.has(SCAN_QR) == true && stateObj?.getBoolean(SCAN_QR) == true) {
                mDecoder?.enableSymbology(DecoderConfigValues.SymbologyID.SYM_QR)
            }
            if (stateObj?.has(SCAN_PDF417) == true && stateObj?.getBoolean(SCAN_PDF417) == true) {
                mDecoder?.enableSymbology(DecoderConfigValues.SymbologyID.SYM_PDF417)
            }
            if (stateObj?.has(SCAN_DATAMATRIX) == true && stateObj?.getBoolean(SCAN_DATAMATRIX) == true) {
                mDecoder?.enableSymbology(DecoderConfigValues.SymbologyID.SYM_DATAMATRIX)
            }
            if (stateObj?.has(SCAN_CODE39) == true && stateObj?.getBoolean(SCAN_CODE39) == true) {
                mDecoder?.enableSymbology(DecoderConfigValues.SymbologyID.SYM_CODE39)
            }

        } catch (e: DecoderException) {
            e.printStackTrace()
        }

    }

    @Throws(PolicyException::class)
    override fun validate(stateObj: JSONObject) {
    }

    companion object {

        private const val DISPLAY_MESSAGE = "display_message"
        private const val STORE_AS = "store_as"
        private const val ON_SCANNED = "on_scanned"
        private const val ON_CANCEL = "on_cancel"
        private const val CANCEL_TEXT = "cancel_text"
        // cancel text is optional
        private const val TAG = "BarcodeScanner"
        private const val SCAN_QR = "scan_qr"
        private const val SCAN_DATAMATRIX = "scan_datamatrix"
        private const val SCAN_PDF417 = "scan_pdf417"
        private const val SCAN_CODE39 = "scan_code39"
        private const val BUTTON_IMAGE = "button_image"

        private const val TYPE = "rlk6735m_barcode_scanner"
    }
}
