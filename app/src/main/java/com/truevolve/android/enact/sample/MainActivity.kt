package com.truevolve.android.enact.sample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.truevolve.android.enact.R
import com.truevolve.android.enact.rlk6735m_barcode_scanner.BarcodeScannerEntryPoint
import com.truevolve.android.enact.take_picture.TakePicture
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.PolicyException
import org.json.JSONException
import java.util.*


class MainActivity : AppCompatActivity() {
    private val policy = "{\n" +
            "  \"start\": {\n" +
            "    \"type\": \"menu\",\n" +
            "    \"on_button\": \"read_a_barcode\"\n" +
            "  },\n" +
            "  \"read_a_barcode\": {\n" +
            "    \"type\": \"rlk6735m_barcode_scanner\",\n" +
            "    \"on_scanned\": \"photo\",\n" +
            "    \"store_as\": \"barcode_read\",\n" +
            "    \"display_message\": \"Please press the big read scan button\",\n" +
            "    \"on_back_pressed\": \"start\",\n" +
            "    \"on_cancel\": \"start\",\n" +
            "    \"scan_pdf417\": true,\n" +
            //                    "    \"resource_to_load\": \"BACKGROUND_AND_BASICS\",\n" +
            "    \"button_image\": \"DRIVER_CARD\"\n" +
            "  },\n" +
            "  \"read_a_barcode1\": {\n" +
            "    \"type\": \"rlk6735m_barcode_scanner\",\n" +
            "    \"on_scanned\": \"read_a_barcode2\",\n" +
            "    \"store_as\": \"barcode_read1\",\n" +
            "    \"display_message\": \"Please press the big read scan button 12345\",\n" +
            "    \"on_back_pressed\": \"start\",\n" +
            "    \"on_cancel\": \"read_a_barcode\",\n" +
            "    \"scan_code39\": true,\n" +
            //                    "    \"resource_to_load\": \"BACKGROUND_AND_BASICS\",\n" +
            "    \"button_image\": \"DRIVER_CARD\"\n" +
            "  },\n" +
            "  \"read_a_barcode2\": {\n" +
            "    \"type\": \"rlk6735m_barcode_scanner\",\n" +
            "    \"on_scanned\": \"photo\",\n" +
            "    \"store_as\": \"barcode_read2\",\n" +
            "    \"display_message\": \"Please press the big read scan button 67890\",\n" +
            "    \"on_back_pressed\": \"start\",\n" +
            "    \"on_cancel\": \"read_a_barcode1\",\n" +
            "    \"scan_pdf417\": true,\n" +
            //                    "    \"resource_to_load\": \"BACKGROUND_AND_BASICS\",\n" +
            "    \"button_image\": \"DRIVER_CARD\"\n" +
            "  },\n" +
            "  \"photo\": {\n" +
            "    \"display_message\": \"Take a photo of something.\",\n" +
            "    \"on_taken_picture\": \"summary\",\n" +
            "    \"store_as\": \"photo\",\n" +
            "    \"type\": \"take_picture\"\n" +
            "  }," +
            "  \"summary\": {\n" +
            "    \"type\": \"summary\",\n" +
            "    \"on_done\": \"SUCCESS\",\n" +
            "    \"barcode_data\": \"barcode_read\",\n" +
            "    \"display_message\": \"Barcode info read is: \",\n" +
            "    \"on_back_pressed\": \"read_a_barcode\"\n" +
            "  }\n" +
            "}"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val listOfControllers = ArrayList<Class<out ActivityBaseController>>()
        listOfControllers.add(Menu::class.java)
        listOfControllers.add(BarcodeScannerEntryPoint::class.java)
        listOfControllers.add(TakePicture::class.java)
        listOfControllers.add(Summary::class.java)

        try {
            val interpreter = Interpreter.setup(MainActivity::class.java, policy, listOfControllers)
            interpreter.start(this)

        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: PolicyException) {
            e.printStackTrace()
        } catch (e: InterpreterException) {
            e.printStackTrace()
        } catch (e: InstantiationException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }

    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume: RESUMING")
    }

    companion object {

        private val TAG = MainActivity::class.java.simpleName
    }
}
