package com.truevolve.android.enact.sample

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.truevolve.android.enact.R
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.ObjectNotFoundException
import com.truevolve.enact.exceptions.PolicyException
import org.json.JSONException
import org.json.JSONObject
import java.lang.reflect.InvocationTargetException

class Summary : ActivityBaseController() {

    override val type: String
        get() = CONTROLLER_TYPE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary)

        val display = findViewById<TextView>(R.id.displayMessageText)
        val barData = findViewById<TextView>(R.id.barcodeDataText)
        val doneButton = findViewById<Button>(R.id.doneButton)

        doneButton.setOnClickListener {
            try {
                stateObj?.let {
                    goToState(it.getString(ON_DONE))
                }
            } catch (e: InterpreterException) {
                e.printStackTrace()
                Interpreter.error(this@Summary, e)
            } catch (e: JSONException) {
                e.printStackTrace()
                Interpreter.error(this@Summary, e)
            }
        }

        try {
            stateObj?.let {
                display.text = it.getString(DISPLAY_MESSAGE)

                val barResult = Interpreter.dataStore.getProperty<ByteArray>(it.getString(BARCODE_DATA))

                if (barResult != null) {
                    barData.text = String(barResult)
                } else {
                    barData.text = "No barcode data available"
                }
            }

        } catch (e: JSONException) {
            e.printStackTrace()
            Interpreter.error(this@Summary, e)
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
            Interpreter.error(this@Summary, e)
        } catch (e: ObjectNotFoundException) {
            e.printStackTrace()
            Interpreter.error(this@Summary, e)
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
            Interpreter.error(this@Summary, e)
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
            Interpreter.error(this@Summary, e)
        }

    }

    @Throws(PolicyException::class)
    override fun validate(stateObj: JSONObject) {
        if (!stateObj.has(DISPLAY_MESSAGE)) {
            Log.e(TAG, "validate: $DISPLAY_MESSAGE was not specified.")
            throw PolicyException("$DISPLAY_MESSAGE was not specified.")
        } else if (!stateObj.has(ON_DONE)) {
            Log.e(TAG, "validate: $ON_DONE was not specified.")
            throw PolicyException("$ON_DONE was not specified.")
        } else if (!stateObj.has(BARCODE_DATA)) {
            Log.e(TAG, "validate: $BARCODE_DATA was not specified.")
            throw PolicyException("$BARCODE_DATA was not specified.")
        }
    }

    companion object {

        private const val TAG = "Summary"
        private const val ON_DONE = "on_done"
        private const val BARCODE_DATA = "barcode_data"
        private const val DISPLAY_MESSAGE = "display_message"

        private const val CONTROLLER_TYPE = "summary"
    }
}
