package com.truevolve.android.enact.sample

import android.os.Bundle
import android.util.Log
import android.view.View

import com.truevolve.android.enact.R
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.PolicyException

import org.json.JSONException
import org.json.JSONObject

class Menu : ActivityBaseController() {

    override val type: String
        get() = CONTROLLER_TYPE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        findViewById<View>(R.id.readButton).setOnClickListener {
            try {
                stateObj?.let {
                    goToState(it.getString(ON_BUTTON))
                }
            } catch (e: InterpreterException) {
                e.printStackTrace()
                Interpreter.error(this@Menu, e)
            } catch (e: JSONException) {
                e.printStackTrace()
                Interpreter.error(this@Menu, e)
            }
        }
    }

    @Throws(PolicyException::class)
    override fun validate(stateObj: JSONObject) {
        if (!stateObj.has(ON_BUTTON)) {
            Log.e(TAG, "validate: state object does not have $ON_BUTTON which is mandatory")
            throw PolicyException("State object does not have $ON_BUTTON which is mandatory")
        }
    }

    companion object {

        private const val TAG = "Menu"
        private const val ON_BUTTON = "on_button"
        private const val CONTROLLER_TYPE = "menu"
    }
}
